import gulp from "gulp";
import run from "gulp-run";
import Browser from "browser-sync";
import jsonModify from "gulp-json-modify";
let exec = require("child_process").exec;
import webpack from "webpack";
import webpackDevMiddleware from "webpack-dev-middleware";
import webpackHotMiddleware from "webpack-hot-middleware";

import webpackConfig from "../config/webpack.config";

const browser = Browser.create();
const bundler = webpack(webpackConfig);

export function server() {
  gulp
    .src(["./package.json"])
    .pipe(
      jsonModify({
        key: "apiUrl",
        value: "http://localhost:3000"
      })
    )
    .pipe(gulp.dest("../"));
  // let config = {
  //   server: "public",
  //   middleware: [
  //     webpackDevMiddleware(bundler, {
  //       noInfo: true,
  //       publicPath: webpackConfig.output.publicPath,
  //       silent: true,
  //       stats: "errors-only"
  //     }),
  //     webpackHotMiddleware(bundler)
  //   ]
  // };

  // browser.init();
  console.log("Compiling");
  run("npm start");
  gulp.watch("src/**/*.js").on("change", () => browser.reload());
  gulp.watch("src/**/*.css").on("change", () => browser.reload());
  gulp.watch("src/**/*.html").on("change", () => browser.reload());
}
