import isEmpty from 'lodash/isEmpty';
import { SET_CURRENT_USER, SAVE_NEWS } from '../actions/appActions';
const initialState = {
	auth: {
		isAuthenticated: false,
		user: {},
	},
};

function appReducer(state = initialState, action) {
	switch (action.type) {
		case SET_CURRENT_USER:
			return Object.assign(...state, {
				auth: {
					isAuthenticated: !isEmpty(action.user),
					user: action.user,
				},
			});
		default:
			return state;
	}
}

export default appReducer;
