import { SAVE_NEWS, SAVE_MOVIES_DATA, SAVE_MOVIE_DATA, SAVE_COMMENT, SAVE_SINGLE_NEWS } from '../actions/appActions';

const initialState = {
	news: [],
	single_news: {},
	movies: [],
	movie: {},
};

function homeReducer(state = initialState, action) {
	switch (action.type) {
		case SAVE_NEWS:
			return Object.assign({}, ...state, {
				news: action.data,
			});
		case SAVE_SINGLE_NEWS:
			return Object.assign({}, ...state, {
				single_news: action.data,
			});
		case SAVE_MOVIES_DATA:
			return {
				...state,
				movies: action.data,
			};
		case SAVE_MOVIE_DATA:
			return {
				...state,
				movie: action.data,
			};
		default:
			return state;
	}
}

export default homeReducer;
