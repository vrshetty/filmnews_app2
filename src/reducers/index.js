import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import appReducer from './appReducers';
import homeReducer from './homeReducers';
import settingsReducer from '../reducers/settingsReducers';

const rootReducer = combineReducers({
	appReducer,
	homeReducer,
	settingsReducer,
	routing: routerReducer,
});

export default rootReducer;
