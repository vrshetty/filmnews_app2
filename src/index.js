import 'babel-polyfill';
import 'isomorphic-fetch';
import React from 'react';
import ReactDOM from 'react-dom';
import jwtDecode from 'jwt-decode';
import { syncHistoryWithStore, routerMiddleware } from 'react-router-redux';
import { Router, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './reducers';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import routes from './routes';
import { loadState, saveState } from './utils/localStorage';
import throttle from 'lodash/throttle';
import { setAuthorizationToken } from './utils/secure';
import { setCurrentUser } from './actions/appActions';

import jquery from 'jquery';

window.jQuery = jquery;

// Can use localStorage to persist state as well
const persistedState = loadState();
const preloadedState = window.__PRELOADED_STATE__;

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

let store = createStore(
	rootReducer,
	preloadedState,
	composeEnhancers(applyMiddleware(thunk, promise, routerMiddleware(browserHistory))),
);
let history = syncHistoryWithStore(browserHistory, store);

store.subscribe(
	throttle(() => {
		saveState(store.getState());
	}, 1500),
);

const token = localStorage.token;
if (token && token !== 'undefined') {
	setAuthorizationToken(token);
	const details = jwtDecode(token);
	store.dispatch(setCurrentUser(details));
}

ReactDOM.render(
	<Provider store={store}>
		<Router history={history} routes={routes(store)} onUpdate={() => window.scrollTo(0, 0)} />
	</Provider>,
	document.getElementById('root'),
);

// Not working properly with React Router: https://github.com/gaearon/react-hot-loader/issues/249
/*
import { AppContainer } from "react-hot-loader";
if (module.hot) {
  module.hot.accept(Root, () => {
    ReactDOM.render(
      <AppContainer>
        <Root store={ store } />
      </AppContainer>,
      document.getElementById("root")
    );
  });
}
*/
