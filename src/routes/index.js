import App from '../containers/App/index';
import Home from '../containers/Home/index';
import Settings from '../containers/Settings/index';
import Logout from '../containers/Logout/index';
import NotFound from '../containers/NotFound/index';

import { requireAuth } from "../utils/secure";
import SingleMovie from "../containers/Movies/single";
import SingleNews from "../containers/News/single";
import AllMovies from "../containers/Movies/all";
import AllNews from "../containers/News";
const rootRoute = store => {
  return {
    path: "/",
    component: App,
    indexRoute: { component: Home, onEnter: requireAuth(store, null) },
    childRoutes: [
      { path: "movies/:id", component: SingleMovie },
      { path: "news/:id", component: SingleNews },
      { path: "movies", component: AllMovies },
      { path: "news", component:  AllNews },
      {
        path: "settings",
        component: Settings,
        onEnter: requireAuth(store, "/")
      },
      { path: "logout", component: Logout },
      { path: "*", component: NotFound }
    ]
  };
};

export default rootRoute;
