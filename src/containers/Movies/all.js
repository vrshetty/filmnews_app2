import React from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';

import { fetchMovies } from "../../actions/appActions";
import MovieCard from "../../components/MovieCard";

class Movies extends React.Component {
    constructor(props) {
        super(props);
    }
    componentWillMount() {
        this.props.fetchMovies();
    }
    render() {
        return (
    <div className="general container">
        <div className="col-sm-2">

        </div>
        <div className="col-sm-12">
            {this.props.movies && this.props.movies.map((movie, i) => {
               return (
                <div className="col-sm-4" key={i}>
                    <MovieCard
                       id={movie["_id"]}
                        movie={movie}
                        title={movie.title}
                        image={movie.thumbnail}
                        celebrities={movie.celebrities}
                    />
                </div>
               )
           })}
        </div>
    </div>
        )
    }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchMovies: bindActionCreators(fetchMovies, dispatch),
  }
}

export default connect(
  state => ({
    movies: state.homeReducer.movies,
  }),
 mapDispatchToProps
)(Movies);