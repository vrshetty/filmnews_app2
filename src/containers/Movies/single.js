import React, { Component } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { bindActionCreators } from 'redux';

// Actions
import { fetchSingleMovie, addMoviesComment } from '../../actions/appActions';

// Reusable Components
import Card from '../../components/MovieCard/single';
import MovieInfo from '../../components/MovieCard/info';
import Comments from '../../components/MovieCard/comments';

class SingleMovie extends Component {
	constructor(props, context) {
		super(props, context);
	}
	componentWillMount() {
		this.props.fetchSingleMovie(this.props.params.id);
	}

	render() {
		return (
			<div>
				<Helmet
					title="FilmiFeed - Movies Entertainment"
					meta={[
						{ property: 'og:url', content: '' },
						{
							property: 'og:title',
							content: 'FilmiFeed - Movies Entertainment',
						},
						{ property: 'og:description', content: '' },
						{ name: 'description', content: '' },
					]}
				/>
				<div className="single-page-agile-main">
					<div className="container">
						<div className="single-page-agile-info">
							<div className="show-top-grids-filmifeedagile">
								<div className="col-sm-12 single-left">
									{this.props.movie && (
										<div className="song">
											<div className="trailer_background image overlay" id>
												<div id="thumbnail" className="col-xs-3">
													<Card image={this.props.movie.thumbnail} />
												</div>
												<MovieInfo
													title={this.props.movie.title}
													category={this.props.movie.category}
													celebrities={this.props.movie.celebrities}
													thumbnail={this.props.movie.thumbnail}
													year={this.props.movie.year}
													genre={this.props.movie.genre}
													created_at={this.props.movie.created_at}
													comments={this.props.movie.comments}
												/>
												<div id="description" className="col-xs-3">
													<p>{this.props.movie.description}</p>
												</div>
											</div>
										</div>
									)}
									<div>
										<div className="clearfix"> </div>
										<div>
											<div className="clearfix"> </div>
											<Comments
												movieId={this.props.params.id}
												addMoviesComment={this.props.addMoviesComment}
												userName={this.props.userName}
												movie={this.props.movie}
												isAuthenticated={this.props.isAuthenticated}
											/>
										</div>
									</div>
								</div>
								<div className="clearfix"> </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

function mapDispatchToProps(dispatch) {
	return {
		fetchSingleMovie: bindActionCreators(fetchSingleMovie, dispatch),
		addMoviesComment: bindActionCreators(addMoviesComment, dispatch),
	};
}
export default connect(
	state => ({
		news: state.homeReducer.news,
		movie: state.homeReducer.movie,
		userName: state.appReducer.auth.user.username,
		isAuthenticated: state.appReducer.auth.isAuthenticated,
	}),
	mapDispatchToProps,
)(SingleMovie);
