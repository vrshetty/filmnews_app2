import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { logoutUser } from '../../actions/appActions';

class Logout extends React.Component {
	componentDidMount() {
		this.props.logoutUser();
		this.props.onRedirect('/');
	}

	render() {
		return null;
	}
}

export default connect(null, dispatch => ({
	onRedirect: path => {
		dispatch(push(path));
	},
	logoutUser: () => {
		dispatch(logoutUser());
	},
}))(Logout);
