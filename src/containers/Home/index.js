import React, { Component, PropTypes } from "react";
import moment from "moment";
import { connect } from "react-redux";
import Helmet from "react-helmet";
import { Link } from "react-router";

import { fetchNews, fetchMovies } from "../../actions/appActions";

import { categoryLists, countryGenreList } from "../../utils/lists";
import NewsCard from "../../components/NewsCard";
import MovieCard from "../../components/MovieCard";

class Home extends Component {
  constructor(props) {
    super(props);
    // this.setTabState =  this.setTabState.bind(this);
    this.state = {
      moviesTab: 'hollywood'
    }
  }
  componentDidMount() {
    //call api to fetch news and movies
    this.props.fetchNews();
    this.props.fetchMovies();
  }
  setTabState = (target, value) => {
    this.setState({
      [target]: value
    })
  }
  render() {
    return (
      <div>
        <Helmet
          title="FilmiFeed - Movies Entertainment"
          meta={[
            { property: "og:url", content: "" },
            {
              property: "og:title",
              content: "FilmiFeed - Movies Entertainment"
            },
            { property: "og:description", content: "" },
            { name: "description", content: "" }
          ]}
        />
        <div className="general container">
          {categoryLists &&
            categoryLists.map((category, i) =>
              <div key={i} className="col-md-6">
                <h4 className="latest-text w3_latest_text">
                  {category.name}
                </h4>
                <div
                  className="bs-example bs-example-tabs"
                  role="tabpanel"
                  data-example-id="togglable-tabs"
                >
                  <ul id="myTab" className="nav nav-tabs" role="tablist">
                    {countryGenreList && countryGenreList.length > 0 &&
                      countryGenreList.map((country_genre, i) =>
                        <li
                            key={i}
                            role="presentation"
                            className={i === 0 ? "active" : ""}
                          >
                          <Link
                            to={`#${country_genre.value}_${category.value}`}
                            id={`${country_genre.value}_${category.value}-tab`}
                            role="tab"
                            data-toggle="tab"
                            aria-controls={`${country_genre.value}_${category.value}`}
                            aria-expanded="true"
                          >
                            {country_genre.name}
                          </Link>
                        </li>
                      )}
                  </ul>
                  <div id="myTabContent" className="tab-content">
                    {countryGenreList &&
                      countryGenreList.map((country_genre, i) =>
                        <div
                          key={i}
                          role="tabpanel"
                          className={`tab-pane fade ${i === 0
                            ? "active"
                            : ""} in`}
                          id={`${country_genre.value}_${category.value}`}
                          aria-labelledby={`${country_genre.value}_${category.value}-tab`}
                        >
                          {this.props.news &&
                            this.props.news
                              .filter(
                                news =>
                                  news.country_genre === country_genre.value &&
                                  category.value === news.category
                              )
                              .map((news, i) =>
                                <div>
                                  <div className="col-md-6">
                                    <NewsCard
                                      key={i}
                                      id={news._id}
                                      width={"100%"}
                                      subTitle={news.title}
                                      image={news.featured_image}
                                      text={moment(news.created_at).format(
                                        "MMM Do, YYYY hh:mm"
                                      )}
                                    />
                                  </div>
                                  <div className="col-md-6">
                                    <NewsCard
                                      key={i}
                                      width={"100%"}
                                      id={news._id}
                                      subTitle={news.title}
                                      image={news.featured_image}
                                      text={moment(news.created_at).format(
                                        "MMM Do, YYYY hh:mm"
                                      )}
                                    />
                                  </div>
                                  <hr />
                                  <ul className="nav nav-tabs">
                                    <li className="pull-right">
                                      <a id="label-purple">See All</a>
                                    </li>
                                  </ul>
                                </div>
                              )}
                              {this.props.news &&
                                this.props.news
                                  .filter(
                                    news =>
                                      news.country_genre === country_genre.value &&
                                      category.value === news.category
                                  ).length === 0 &&
                                        <h3 className="no_content">{country_genre.value} news is not available</h3>
                                      }
                        </div>
                      )}
                  </div>
                </div>
              </div>
            )}
          <div className="col-xs-12">
            <h4 className="latest-text w3_latest_text">Movies Review</h4>
            <div
              className="bs-example bs-example-tabs"
              role="tabpanel"
              data-example-id="togglable-tabs"
            >
              <ul id="myTab" className="nav nav-tabs" role="tablist">
                {countryGenreList &&
                  countryGenreList.map((country_genre, i) =>
                    <li
                      key={i}
                      role="presentation"
                      className={i === 0 ? "active" : ""}
                    >
                      <Link
                        onClick={() => this.setTabState('moviesTab', country_genre.value)}
                        id={`${country_genre.value}-tab_movies`}
                        role="tab"
                        data-toggle="tab"
                        aria-controls={`${country_genre.value}_movies`}
                        aria-expanded="true"
                      >
                        {country_genre.name}
                      </Link>
                    </li>
                  )}
              </ul>
              <div id="myTabContent" className="tab-content">
                {countryGenreList &&
                  countryGenreList.map((country_genre, i) =>
                    <div
                      key={i}
                      role="tabpanel"
                      className={`tab-pane fade ${this.state.moviesTab === country_genre.value ? "active" : ""} in`}
                      id={`${country_genre.value}_movies`}
                      aria-labelledby={`${country_genre.value}_movies`}
                    >
                      {this.props.movies &&
                        this.props.movies
                          .filter(
                            movies => movies.category === country_genre.value
                          )
                          .map((movie, i) =>
                            <div className="col-sm-4" key={i}>
                              <MovieCard
                                id={movie["_id"]}
                                movie={movie}
                                title={movie.title}
                                image={movie.thumbnail}
                                celebrities={movie.celebrities}
                              />
                            </div>
                          )}
                          {this.props.movies &&
                            this.props.movies
                            .filter(
                              movies => movies.category === country_genre.value
                            ).length === 0 ?
                              <h3 className="no_content"> {country_genre.value} movies is not available   </h3> : <ul id="see_all" className="nav nav-tabs">
                              <li className="pull-right">
                                <a id="label-purple">See All</a>
                              </li>
                            </ul>
                            }
                    </div>
                  )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    news: state.homeReducer.news,
    movies: state.homeReducer.movies
  }),
  dispatch => ({
    fetchNews: () => {
      dispatch(fetchNews());
    },
    fetchMovies: () => {
      dispatch(fetchMovies());
    }
  })
)(Home);
