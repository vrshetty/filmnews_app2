import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import request from 'superagent';
import DropZone from 'react-dropzone';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';

const CLOUDINARY_UPLOAD_PRESET = 'ykipp5fz';
const CLOUDINARY_UPLOAD_URL = 'https://api.cloudinary.com/v1_1/dld2dgb79/upload';

import CreateNews from '../../components/Admin/CreateNews';
import CreateMovie from '../../components/Admin/CreateMovie';
import * as SettingsActions from '../../actions/settingsActions';

class Settings extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div>
				<Helmet
					title="Settings - FilmiFeed"
					meta={[
						{ property: 'og:url', content: '' },
						{ property: 'og:title', content: 'Settings - FilmiFeed' },
						{ property: 'og:description', content: '' },
						{ name: 'description', content: '' },
					]}
				/>
				<div className="container">
					<div className="agileits-news-top">
						<ol className="breadcrumb">
							<li>
								<a href="index.html">Home</a>
							</li>
							<li className="active">Admin Settings</li>
						</ol>
					</div>
					<div className="agileinfo-news-top-grids">
						<div className="col-md-8 wthree-top-news-left">
							<div className="wthree-news-left">
								<div className="tabbable tabs-left">
									<ul className="nav nav-tabs">
										<li className="active">
											<a href="#news" data-toggle="tab">
												News
											</a>
										</li>
										<li>
											<a href="#movies" data-toggle="tab">
												Movies
											</a>
										</li>
										<li>
											<a href="#clebrities" data-toggle="tab">
												Celebrities
											</a>
										</li>
										{/* <li>
                      <a href="#reviews" data-toggle="tab">
                        Contact
                      </a>
                    </li> */}
									</ul>
									<div className="tab-content">
										<div className="tab-pane active" id="news">
											<div className="card">
												<CreateNews
													saveNews={this.props.saveNews}
													currentUser={this.props.user}
												/>
											</div>
										</div>
										<div className="tab-pane" id="movies">
											<div className="card">
												<CreateMovie saveMovies={this.props.saveMovies} />
											</div>
										</div>
										<div className="tab-pane" id="clebrities">
											<div className>
												<h1>Celebrities Tab</h1>
												<p>Not Implemented Yet</p>
											</div>
										</div>
										{/* <div className="tab-pane" id="reviews">
                      <div className>
                        <h1>Contact Tab</h1>
                        <p>
                          deserve editor attention because they are the most
                          important for an encyclopedia to have, as determined
                          by the community of participating editors. They may
                          also be of interest to readers as an alternative to
                          lists of overview articles.
                        </p>
                      </div>
                    </div> */}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	counter: state.settingsReducer.counter,
	user: state.appReducer.auth.user,
});

const mapDispatchToProps = dispatch => {
	return bindActionCreators(SettingsActions, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
