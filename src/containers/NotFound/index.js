import React, { Component, PropTypes } from "react";
import { Link } from "react-router";
import styles from "./styles.css";

class NotFound extends Component {
  render() {
    return (
      <div className={ `${ styles.content } ${ styles.container }` }>
        <h1>404</h1>
        <h2>Page not found!</h2>
        <p>
          <Link to="/"> Back to the home page</Link>
        </p>
      </div>
    );
  }
}

export default NotFound;
