import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';

import NewsPost from '../../components/NewsCard/single';
import NewsComments from '../../components/NewsCard/comments';
import { fetchSingleNews, addNewsComment } from '../../actions/appActions';

class SingleNews extends Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			comment: '',
			success: false,
			news: {},
		};
		this.onChange = this.onChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}

	componentDidMount() {
		this.props.fetchSingleNews(this.props.params.id);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.singleNews) {
			const newData = {
				comments: nextProps.singleNews.comments.reverse(),
				...nextProps.singleNews,
			};
			this.setState({
				news: newData,
			});
		}
	}

	onChange(event) {
		event.preventDefault();
		this.setState({
			comment: event.target.value,
		});
	}

	onSubmit(event) {
		event.preventDefault();
		let comment = {
			userId: this.props.currentUser.id,
			username: this.props.currentUser.username,
			comment: this.state.comment,
			created_date: new Date(),
		};
		if (this.state.comment.trim() && this.props.singleNews) {
			this.props.addNewsComment(this.props.singleNews._id, comment).then(() => {
				this.setState(
					{
						success: true,
						comment: '',
					},
					() =>
						setTimeout(() => {
							this.setState({
								success: false,
							});
						}, 2000),
				);
			});
		}
	}
	render() {
		return (
			<div>
				<Helmet
					title="FilmiFeed - Movies Entertainment"
					meta={[
						{ property: 'og:url', content: '' },
						{
							property: 'og:title',
							content: 'FilmiFeed - Movies Entertainment',
						},
						{ property: 'og:description', content: '' },
						{ name: 'description', content: '' },
					]}
				/>
				<div className="faq">
					<div className="container">
						<div className="agileits-news-top">
							<ol className="breadcrumb">
								<li>
									<a href="index.html">Home</a>
								</li>
								<li>
									<a href="news.html">News</a>
								</li>
								<li className="active">Single</li>
							</ol>
						</div>
						<div className="agileinfo-news-top-grids">
							<div className="col-md-8 wthree-top-news-left">
								{/* News Body */}
								{this.state.news && <NewsPost news={this.state.news} />}
								<div className="wthree-related-news-left">
									<h4>Related News</h4>
									<div className="wthree-news-top-left">
										<div className="col-md-6 w3-agileits-news-left">
											<div className="col-sm-5 wthree-news-img">
												<a href="news-single.html">
													<img src="/images/m1.jpg" alt />
												</a>
											</div>
											<div className="col-sm-7 wthree-news-info">
												<h5>
													<a href="news-single.html">
														Lorem ipsum dolor sit amet, consectetur adipiscing elit.
													</a>
												</h5>
												<p>
													Sed tristique mattis fermentum. Etiam semper aliquet massa, id
													tempus massa mattis eget.
												</p>
												<ul>
													<li>
														<i className="fa fa-clock-o" aria-hidden="true" /> 24/09/2016
													</li>
													<li>
														<i className="fa fa-eye" aria-hidden="true" /> 2642
													</li>
												</ul>
											</div>
											<div className="clearfix"> </div>
										</div>
										<div className="col-md-6 w3-agileits-news-left">
											<div className="col-sm-5 wthree-news-img">
												<a href="news-single.html">
													<img src="/images/m2.jpg" alt />
												</a>
											</div>
											<div className="col-sm-7 wthree-news-info">
												<h5>
													<a href="news-single.html">
														Lorem ipsum dolor sit amet, consectetur adipiscing elit.
													</a>
												</h5>
												<p>
													Sed tristique mattis fermentum. Etiam semper aliquet massa, id
													tempus massa mattis eget.
												</p>
												<ul>
													<li>
														<i className="fa fa-clock-o" aria-hidden="true" /> 24/09/2016
													</li>
													<li>
														<i className="fa fa-eye" aria-hidden="true" /> 2642
													</li>
												</ul>
											</div>
											<div className="clearfix"> </div>
										</div>
										<div className="clearfix"> </div>
									</div>
								</div>
								{/* agile-comments */}
								{this.state.news && (
									<NewsComments
										onChange={this.onChange}
										onSubmit={this.onSubmit}
										success={this.state.success}
										comment={this.state.comment}
										commentsData={this.state.news.comments}
										isAuthenticated={this.props.isAuthenticated}
									/>
								)}
								{/* //agile-comments */}
								<div className="news-related" />
							</div>
							<div className="col-md-4 wthree-news-right">
								{/* news-right-top */}
								<div className="news-right-top">
									<div className="wthree-news-right-heading">
										<h3>Updated News</h3>
									</div>
									<div className="wthree-news-right-top">
										<div className="news-grids-bottom">
											{/* date */}
											<div id="design" className="date">
												<div id="cycler">
													<div className="date-text">
														<a href="news-single.html">August 15,2016</a>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
													</div>
													<div className="date-text">
														<a href="news-single.html">
															July 08,2016
															<span className="blinking">
																<img src="/images/new.png" alt />
															</span>
														</a>
														<p>
															Nullam non turpis sit amet metus tristique egestas et et
															orci.
														</p>
													</div>
													<div className="date-text">
														<a href="news-single.html">February 15,2016</a>
														<p>Duis venenatis ac ipsum vel ultricies in placerat quam</p>
													</div>
													<div className="date-text">
														<a href="news-single.html">
															January 15,2016
															<span className="blinking">
																<img src="/images/new.png" alt />
															</span>
														</a>
														<p>
															Pellentesque ullamcorper fringilla ipsum, ornare dapibus
															velit volutpat sit amet.
														</p>
													</div>
													<div className="date-text">
														<a href="news-single.html">September 24,2016</a>
														<p>In lobortis ipsum mi, ac imperdiet elit pellentesque at.</p>
													</div>
													<div className="date-text">
														<a href="news-single.html">August 15,2016</a>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
													</div>
													<div className="date-text">
														<a href="news-single.html">
															July 08,2016
															<span className="blinking">
																<img src="/images/new.png" alt />
															</span>
														</a>
														<p>
															Nullam non turpis sit amet metus tristique egestas et et
															orci.
														</p>
													</div>
													<div className="date-text">
														<a href="news-single.html">February 15,2016</a>
														<p>Duis venenatis ac ipsum vel ultricies in placerat quam</p>
													</div>
													<div className="date-text">
														<a href="news-single.html">
															January 15,2016
															<span className="blinking">
																<img src="/images/new.png" alt />
															</span>
														</a>
														<p>
															Pellentesque ullamcorper fringilla ipsum, ornare dapibus
															velit volutpat sit amet.
														</p>
													</div>
													<div className="date-text">
														<a href="news-single.html">September 24,2016</a>
														<p>In lobortis ipsum mi, ac imperdiet elit pellentesque at.</p>
													</div>
												</div>
											</div>
											{/* //date */}
										</div>
									</div>
								</div>
								{/* //news-right-top */}
								{/* news-right-bottom */}
								<div className="news-right-bottom">
									<div className="wthree-news-right-heading">
										<h3>Top News</h3>
									</div>
									<div className="news-right-bottom-bg">
										<div className="news-grids-bottom">
											<div className="top-news-grid">
												<div className="top-news-grid-heading">
													<a href="news-single.html">
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
														rutrum ac nulla
													</a>
												</div>
												<div className="filmifeeds-news-t-grid top-t-grid">
													<ul>
														<li>
															<a href="#">
																<i className="fa fa-clock-o" /> 1h
															</a>
														</li>
														<li>
															<a href="#">
																<i className="fa fa-user" /> Vivamus nibh
															</a>
														</li>
													</ul>
												</div>
											</div>
											<div className="top-news-grid">
												<div className="top-news-grid-heading">
													<a href="news-single.html">
														Duis orci enim, rutrum vel sodales ut, tincidunt nec turpis.
													</a>
												</div>
												<div className="filmifeeds-news-t-grid top-t-grid">
													<ul>
														<li>
															<a href="#">
																<i className="fa fa-clock-o" /> 3h
															</a>
														</li>
														<li>
															<a href="#">
																<i className="fa fa-user" /> Cras pretium
															</a>
														</li>
													</ul>
												</div>
											</div>
											<div className="top-news-grid">
												<div className="top-news-grid-heading">
													<a href="news-single.html">
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
														rutrum ac nulla
													</a>
												</div>
												<div className="filmifeeds-news-t-grid top-t-grid">
													<ul>
														<li>
															<a href="#">
																<i className="fa fa-clock-o" /> 1h
															</a>
														</li>
														<li>
															<a href="#">
																<i className="fa fa-user" /> Vivamus nibh
															</a>
														</li>
													</ul>
												</div>
											</div>
											<div className="top-news-grid">
												<div className="top-news-grid-heading">
													<a href="news-single.html">
														Duis orci enim, rutrum vel sodales ut, tincidunt nec turpis.
													</a>
												</div>
												<div className="filmifeeds-news-t-grid top-t-grid">
													<ul>
														<li>
															<a href="#">
																<i className="fa fa-clock-o" /> 3h
															</a>
														</li>
														<li>
															<a href="#">
																<i className="fa fa-user" /> Cras pretium
															</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								{/* //news-right-bottom */}
							</div>
							<div className="clearfix"> </div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
export default connect(
	state => ({
		singleNews: state.homeReducer.single_news,
		currentUser: state.appReducer.auth.user,
		isAuthenticated: state.appReducer.auth.isAuthenticated,
	}),
	dispatch => ({
		fetchSingleNews: id => {
			dispatch(fetchSingleNews(id));
		},
		addNewsComment: (id, data) => dispatch(addNewsComment(id, data)),
	}),
)(SingleNews);
