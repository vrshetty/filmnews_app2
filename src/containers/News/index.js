import React from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import moment from "moment";

import { fetchNews } from "../../actions/appActions";
import NewsCard from "../../components/NewsCard";

class News extends React.Component {
    constructor(props) {
        super(props);
    }
    componentWillMount() {
      this.props.fetchNews();
    }
    render() {
        return (
    <div className="general container">
        <div className="col-sm-2">
        </div>
        <div className="col-sm-10">
            { this.props.news.map((news, i) => {
               return (
                <div className="col-sm-4" key={i}>
                    <NewsCard
                        key={i}
                        subTitle={news.title}
                        image={news.featured_image}
                        text={moment(news.created_at).format(
                        "MMM Do, YYYY hh:mm"
                        )}
                    />
                </div>
               )
           })}
        </div>
    </div>
        )
    }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchNews: bindActionCreators(fetchNews, dispatch),
  }
}

export default connect(
  state => ({
    news: state.homeReducer.news,
  }),
 mapDispatchToProps
)(News);