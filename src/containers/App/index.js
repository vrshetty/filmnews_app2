import React, { Component, PropTypes } from 'react';
import { ToastContainer } from 'react-toastify';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { resetNext, setCurrentUser, saveUserDetails, loginUser } from '../../actions/appActions';
import styles from './styles.css';
import Helmet from 'react-helmet';
import Authentication from '../../components/Authentication';
import Header from '../../components/Header';
import Footer from '../../components/Footer';

// const $ = window.jQuery;

class App extends Component {
	render() {
		return (
			<div className={styles.main}>
				<Helmet
					title="React Redux Starter Kit"
					meta={[
						{ property: 'og:url', content: '' },
						{ property: 'og:type', content: 'website' },
						{ property: 'og:title', content: 'React Redux Starter Kit' },
						{ property: 'og:site_name', content: 'React Redux Starter Kit' },
						{
							property: 'og:description',
							content:
								'A starter boilerplate for universal modular React apps that includes Redux, Express, Mongoose, Passport and local jwt hash-based auth',
						},
						{
							name: 'description',
							content:
								'A starter boilerplate for universal modular React apps that includes Redux, Express, Mongoose, Passport and local jwt hash-based auth',
						},
					]}
				/>
				<Header auth={this.props.auth} />
				<Authentication
					setCurrentUser={this.props.setCurrentUser}
					saveUserDetails={this.props.saveUserDetails}
					loginUser={this.props.loginUser}
				/>
				{this.props.children}
				<ToastContainer hideProgressBar={true} newestOnTop={true} autoClose={3000} />
				<Footer />
			</div>
		);
	}
}

export default connect(
	state => ({
		auth: state.appReducer.auth,
	}),
	dispatch => ({
		onRedirect: path => {
			dispatch(push(path));
		},
		onResetNext: () => {
			dispatch(resetNext());
		},
		setCurrentUser: user => {
			dispatch(setCurrentUser(user));
		},
		saveUserDetails: user => dispatch(saveUserDetails(user)),
		loginUser: user => dispatch(loginUser(user)),
	}),
)(App);
