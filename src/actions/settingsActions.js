import axios from 'axios';

export function incrementCounter() {
	return {
		type: 'INCREMENT_COUNTER',
	};
}

export function decrementCounter() {
	return {
		type: 'DECREMENT_COUNTER',
	};
}

export function saveNews(postData) {
	return (dispatch, getState) => axios.post(`${process.env.apiUrl}/api/news`, postData).then(response => {});
}

export function saveMovies(postData) {
	return (dispatch, getState) => axios.post(`${process.env.apiUrl}/api/movies`, postData).then(response => {});
}

export function requestCounter() {
	return {
		type: 'REQUEST_COUNTER',
	};
}

export function receiveCounter(counter) {
	return {
		type: 'RECIEVE_COUNTER',
		counter: counter,
	};
}
