import axios from 'axios';
import jwtDecode from 'jwt-decode';
import { setAuthorizationToken } from '../utils/secure';

export const SET_NEXT = 'SET_NEXT';
export const RESET_NEXT = 'RESET_NEXT';
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const SAVE_NEWS = 'SAVE_NEWS';
export const SAVE_SINGLE_NEWS = 'SAVE_SINGLE_NEWS';
export const SAVE_MOVIES_DATA = 'SAVE_MOVIES_DATA';
export const SAVE_MOVIE_DATA = 'SAVE_MOVIE_DATA';

export function resetNext() {
	return {
		type: RESET_NEXT,
	};
}
export function setNext(next) {
	return {
		type: SET_NEXT,
		next,
	};
}

/**
 * action dispatched on creating new user success
 *
 * @export
 * @param {any} user
 * @returns {Object} json object
 */
export function setCurrentUser(user) {
	return {
		type: SET_CURRENT_USER,
		user,
	};
}

/**
 * SignUp - SignUp Action
 * @param  {object} userDetails contains the user details
 * @return {object} return an object
 */
export function saveUserDetails(userDetails) {
	return dispatch =>
		axios.post(`${process.env.apiUrl}/api/user/register`, userDetails).then(({ data }) => {
			const token = data.token;
			localStorage.setItem('token', token);
			setAuthorizationToken(token);
			const decodedData = jwtDecode(token);
			dispatch(setCurrentUser(decodedData));
		});
}

/**
 * Login - Login Action
 * @param  {object} userDetails contains the user details
 * @return {object} return an object
 */
export function loginUser(userDetails) {
	return dispatch =>
		axios.post(`${process.env.apiUrl}/api/user/login`, userDetails).then(({ data }) => {
			const token = data.token;
			localStorage.setItem('token', token);
			setAuthorizationToken(token);
			const decodedData = jwtDecode(token);
			dispatch(setCurrentUser(decodedData));
		});
}

/**
 * Logout - Logout Action
 * @param  {object} userDetails contains the user details
 * @return {object} return an object
 */
export function logoutUser(userDetails) {
	return dispatch => {
		localStorage.removeItem('token');
		dispatch(setCurrentUser({}));
	};
}

export function fetchNews() {
	return dispatch =>
		axios.get(`${process.env.apiUrl}/api/news`).then(({ data }) => {
			dispatch(saveNewsData(data));
		});
}

export function fetchMovies() {
	return dispatch =>
		axios.get(`${process.env.apiUrl}/api/movies`).then(({ data }) => {
			dispatch(saveMoviesData(data));
		});
}

export function fetchSingleMovie(id) {
	return dispatch =>
		axios.get(`${process.env.apiUrl}/api/movies/${id}`).then(({ data }) => {
			dispatch(saveMovieData(data));
		});
}
export function fetchSingleNews(id) {
	return dispatch =>
		axios.get(`${process.env.apiUrl}/api/news/${id}`).then(({ data }) => {
			dispatch(saveSingleNewsData(data));
		});
}

export function saveNewsData(data) {
	return {
		type: SAVE_NEWS,
		data,
	};
}

export function addMoviesComment(postdata) {
	return dispatch =>
		axios
			.patch(`${process.env.apiUrl}/api/movies/${postdata.movieId}/comment`, postdata.comment)
			.then(({ data }) => {
				dispatch(fetchSingleMovie(postdata.movieId));
			});
}
export function saveSingleNewsData(data) {
	return {
		type: SAVE_SINGLE_NEWS,
		data,
	};
}

export function saveMoviesData(data) {
	return {
		type: SAVE_MOVIES_DATA,
		data,
	};
}

export function saveMovieData(data) {
	return {
		type: SAVE_MOVIE_DATA,
		data,
	};
}
export function addNewsComment(id, data) {
	return dispatch =>
		axios.put(`${process.env.apiUrl}/api/news/${id}/comment`, data).then(response => {
			dispatch(fetchSingleNews(id));
		});
}
