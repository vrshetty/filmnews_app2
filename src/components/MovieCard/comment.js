import React from 'react';
import ReactStars from 'react-stars'

const Comment = (props) => {
    return (
        <div className="media-grids">
        <div className="media">
          <div className="media-left">
            <a href="#">
              <img src="/images/user.jpg" title="One movies" alt=" " />
            </a>
          </div>
          <div className="media-body">
            <h6>{props.userName}</h6>
            <div className="comment_rating">
                <ReactStars
                    value={props.rating}
                    edit={false}
                />
            </div><br/>
             <p>
                {props.comment}
            </p>
          </div>
        </div>
      </div>
    )
}

export default Comment;
