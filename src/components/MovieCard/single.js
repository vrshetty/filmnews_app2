import React from "react";
import { Link } from "react-router";
import styles from "./styles.css";

const Card = ({ width, image }) =>
  <div className="filmifeed-movie-gride-agile">
    <Link
      style={{ backgroundImage: `url(${image})` }}
      to="/"
      className="hvr-shutter-out-horizontal"
    >
      <div id="play" />
    </Link>
  </div>;

export default Card;
