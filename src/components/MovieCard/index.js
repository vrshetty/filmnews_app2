import React from "react";
import { Link } from "react-router";

const Card = ({ id, width, title, image, celebrities, text, movie }) =>
  <div className="filmifeed-movie-gride-agile">
    <Link
      style={{ backgroundImage: `url(${image})` }}
      to={{pathname: `/movies/${id}`, state: movie}}
      className="hvr-shutter-out-horizontal"
    >
      <div id="play" style={{}} />
    </Link>
    <div className="mid-1 agileits_filmifeedayouts_mid_1_home">
      <div className="filmifeed-movie-text">
        <h6>
          <Link to={`/movies/${id}`}>
            {title}
          </Link>
          <span className="pull-right">
            <a href="#">
              <i className="fa fa-thumbs-up" aria-hidden="true" />
            </a>
            98%
          </span>
        </h6>
      </div>
      <div className="mid-2 agile_mid_2_home">
        {celebrities &&
          celebrities.map((celebrity, i) =>
            <div key={i} className="chip">
              {celebrity}
            </div>
          )}
        <div className="clearfix" />
      </div>
          <p id="lang">Hindi, English</p>
    </div>
    <div className="ribben">
      <p>NEW</p>
    </div>
  </div>;

export default Card;
