import React from 'react';
import ReactStars from 'react-stars';

import StarRating from '../Rating/';
import Comment from './comment';

class Comments extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			comment: '',
			rating: 0,
			comments: [],
			averageRatings: 0,
			numberOfComments: 0,
			errorMessage: '',
		};
		this.ratingChange = this.ratingChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.onChange = this.onChange.bind(this);
		this.getAveregeRatings = this.getAveregeRatings.bind(this);
	}

	ratingChange(newRating) {
		if (this.state.errorMessage && this.state.rating === 0) {
			this.setState({
				errorMessage: '',
			});
		}
		this.setState({
			rating: newRating,
		});
	}

	onChange(event) {
		if (this.state.errorMessage && this.state.comment.trim()) {
			this.setState({
				errorMessage: '',
			});
		}
		this.setState({
			[event.target.name]: event.target.value,
		});
	}

	onSubmit(event) {
		event.preventDefault();
		if (this.state.rating === 0 && !this.state.comment) {
			this.setState({
				errorMessage: 'Enter a review and rating before you submit',
			});
		} else if (!this.state.comment.trim()) {
			this.setState({
				errorMessage: 'Enter a review before you submit',
			});
		} else if (this.state.rating === 0) {
			this.setState({
				errorMessage: 'Enter a rating before you submit',
			});
		} else {
			const comment = {
				userName: this.props.userName,
				comment: this.state.comment,
				rating: this.state.rating,
			};
			const data = {
				comment,
				movieId: this.props.movieId,
			};
			this.props.addMoviesComment(data).then(() => {
				this.setState({
					comment: '',
					rating: 0,
				});
			});
		}
	}
	getAveregeRatings() {
		let totalRatings = 0;
		this.state.comments.forEach(comment => {
			totalRatings = totalRatings + comment.rating;
		});
		this.state.averageRatings = totalRatings / this.state.comments.length;
		this.state.numberOfComments = this.state.comments.length;
	}

	render() {
		if (this.props.movie) {
			if (Object.keys(this.props.movie).length !== 0) {
				this.state.comments = this.props.movie.comments;
				this.getAveregeRatings();
			}
		}
		const comment_message = this.props.isAuthenticated
			? 'Be the Fist to post a comment'
			: 'Sign in to post a comment';
		return (
			<div className="card all-comments">
				{this.props.isAuthenticated && (
					<div className="mes" role="alert">
						<b>Avg User's Rating</b>
						<i id="star" className="fa fa-star" aria-hidden="true" />
						<span>
							<b>{this.state.averageRatings.toFixed(2)} /5</b> ({this.state.numberOfComments} comments){' '}
						</span>
					</div>
				)}
				{this.state.errorMessage !== '' && (
					<div className="alert alert-danger" role="alert">
						{this.state.errorMessage}
					</div>
				)}
				{this.props.isAuthenticated && (
					<div className="user-star">
						<div className="media-left">
							<a href="#">
								<img src="/images/user.jpg" title="One movies" alt=" " />
							</a>
						</div>
						<div className="star-container">
							<ReactStars value={this.state.rating} size={35} onChange={this.ratingChange} />
						</div>
					</div>
				)}
				{this.props.isAuthenticated ? (
					<div className="all-comments-info">
						<form onSubmit={this.onSubmit}>
							<div className="row">
								<div className="col-md-12">
									<div className="form-group">
										<textarea
											value={this.state.comment}
											name="comment"
											onChange={this.onChange}
											rows={5}
											placeholder="Write a Comment"
											id="formControlsTextarea"
											className="form-control"
										/>
									</div>
								</div>
							</div>
							<button type="submit" className="btn-fill pull-right btn btn-danger">
								Submit
							</button>
						</form>
						{this.state.comments.map(comment => {
							return (
								<Comment
									key={comment._id}
									comment={comment.comment}
									userName={comment.user_name}
									rating={comment.rating}
								/>
							);
						})}
						{this.state.comments.length === 0 && (
							<h2 className="no_content">No Comment yet! Be the first to post</h2>
						)}
					</div>
				) : (
					<h2 className="no_content">SignIn to post a Comment</h2>
				)}
			</div>
		);
	}
}

export default Comments;
