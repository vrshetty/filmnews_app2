import React, { Component } from "react";
import ReactStars from 'react-stars'

export default class MovieInfo extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    let averageRatings = 0
    const {
      title,
      created_at,
      trailer_url,
      genre,
      year,
      category,
      thumbnail,
      celebrities,
      comments
    } = this.props;
    if (comments) {
      let ratingSum = 0;
      comments.forEach((comment) => {
        ratingSum = ratingSum + comment.rating;
      });
      averageRatings = ratingSum/comments.length;
    }
    return (
      <div id="info" className="col-xs-6">
        <div className="heading">
          <h3 id="title">
            {title}
          </h3>
          <span> 01-02-2018 | 1hr 50mins | Hindi, English</span>
          <p>
            Genre: {category}
          </p>
          <p>
            Celebrities:{" "}
            {celebrities &&
              celebrities.map((celebrity, i) =>
                <a key={i}>
                  {celebrity}
                </a>
              )}
          </p>
          <p>Director: Solomon Kingsley</p>
        </div>
        <div className="action_btn col-xs-6">
          <button id="rate" className="btn btn-sm btn-info col-xs-6">
            <i className="fa fa-star" aria-hidden="true" />
            Rate This
          </button>
          <button className="btn btn-sm btn-info col-xs-6 star-button">
             <h6>{averageRatings.toFixed(2)}</h6>
            <ReactStars
              value={averageRatings}
              edit={false}
            />
          </button>
        </div>
      </div>
    );
  }
}
