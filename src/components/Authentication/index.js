import React from "react";
import axios from "axios";
import jwt from "jsonwebtoken";
import { toast } from "react-toastify";

class Authentication extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    login: {
      username: "",
      password: ""
    },
    register: {
      username: "",
      fullname: "",
      password: "",
      email: ""
    }
  };
  onInputChange(type, event) {
    let state = this.state[type];
    state[event.target.name] = event.target.value;
    this.setState({
      [type]: state
    });
  }
  registerUser(event) {
    event.preventDefault();
    const postData = {
      email: this.state.register.email,
      username: this.state.register.username.trim(),
      fullname: this.state.register.fullname,
      password: this.state.register.password,
      confirm: this.state.register.password
    };
    this.props
      .saveUserDetails(postData)
      .then(() => {
        toast.success(
          `Account created successfully
          You are logged in as ${postData.username}
        `
        );
        $("#myModal").modal("hide");
      })
      .catch(error => toast.error(error.response.data.error));
  }
  loginUser(event) {
    event.preventDefault();
    const postData = {
      username: this.state.login.username.trim(),
      password: this.state.login.password
    };
    this.props
      .loginUser(postData)
      .then(() => {
        toast.success(`Success, You are logged in as ${postData.username}`);
        $("#myModal").modal("hide");
      })
      .catch(error => toast.error(error.response.data.error));
  }
  render() {
    const { login, register } = this.state;
    return (
      <div
        className="modal video-modal fade"
        id="myModal"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="myModal"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              Sign In &amp; Sign Up
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <section>
              <div className="modal-body">
                <div className="w3_login_module">
                  <div className="module form-module">
                    <div className="toggle">
                      <i className="fa fa-times fa-pencil" />
                      <div className="tooltip">Click Me</div>
                    </div>
                    <div className="form">
                      <h3>Login to your account</h3>
                      <form
                        onSubmit={this.loginUser.bind(this)}
                        action="#"
                        method="post"
                      >
                        <input
                          onChange={this.onInputChange.bind(this, "login")}
                          value={login.username}
                          type="text"
                          name="username"
                          placeholder="Username"
                          required
                        />
                        <input
                          onChange={this.onInputChange.bind(this, "login")}
                          type="password"
                          value={login.password}
                          name="password"
                          placeholder="Password"
                          required
                        />
                        <input type="submit" defaultValue="Login" />
                      </form>
                    </div>
                    <div className="form">
                      <h3>Create an account</h3>
                      <form
                        onSubmit={this.registerUser.bind(this)}
                        method="post"
                      >
                        <input
                          onChange={this.onInputChange.bind(this, "register")}
                          value={register.fullname}
                          type="text"
                          name="fullname"
                          placeholder="Full Name"
                          required
                        />
                        <input
                          onChange={this.onInputChange.bind(this, "register")}
                          value={register.username}
                          type="text"
                          name="username"
                          placeholder="Username"
                          required
                        />
                        <input
                          onChange={this.onInputChange.bind(this, "register")}
                          value={register.password}
                          type="password"
                          name="password"
                          placeholder="password"
                          required
                        />
                        <input
                          onChange={this.onInputChange.bind(this, "register")}
                          value={register.email}
                          type="email"
                          name="email"
                          placeholder="Email Address"
                          required
                        />
                        <input
                          onChange={this.onInputChange.bind(this, "register")}
                          type="submit"
                          defaultValue="Register"
                        />
                      </form>
                    </div>
                    <div className="cta">
                      <a href="#">Forgot your password?</a>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    );
  }
}

export default Authentication;
