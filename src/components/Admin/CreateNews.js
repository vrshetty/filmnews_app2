import React from 'react';
import Dropzone from 'react-dropzone';
import { toast } from 'react-toastify';
import { categoryLists, countryGenreList } from '../../utils/lists';

class CreateNews extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			title: '',
			content: '',
			thumbnail: null,
			completed: false,
			filename: 'No File has been selected',
			category: '',
			country_genre: '',
		};
	}

	onInputChange(event) {
		let state = this.state;
		state[event.target.name] = event.target.value;
		this.setState(state);
	}

	uploadWidget(event) {
		let self = this;
		cloudinary.openUploadWidget(
			{
				cloud_name: 'dhftkozwd',
                  upload_preset: 's49bhqbg',
				tags: ['news_photos'],
			},
			function(error, result) {
				if (!error) {
					self.setState({
						thumbnail: result[0].secure_url,
						filename: `${result[0].original_filename}.${result[0].format}`,
					});
					toast.success('Image was uploaded succesfully');
				}
			},
		);
	}

	postNews(event) {
		event.preventDefault();
		const postData = {
			title: this.state.title,
			content: this.state.content,
			category: this.state.category,
			country_genre: this.state.country_genre,
			featured_image: this.state.thumbnail,
			author: this.props.currentUser.username,
			created_date: new Date(),
		};
		const { valid, result } = this.isValid(postData);
		if (valid) {
			this.props.saveNews(postData).then(() => toast.success(`News was posted successfully`));
		} else {
			this.setState({
				isValid: false,
			});
		}
	}

	isValid(data) {
		const result = Object.keys(data).filter(key => !data[key]);
		return { valid: result.length === 0, result };
	}

	render() {
		const { title, content, thumbnail, category, country_genre } = this.state;
		const isValid =
			title.length > 0 &&
			content.length > 0 &&
			thumbnail.length > 0 &&
			category.length > 0 &&
			country_genre.length > 0;
		return (
			<div className="row">
				<div className="col-md-12">
					<h4 className="latest-text w3_latest_text">Post a News</h4>
					{!isValid && <div className="alert alert-danger">All fields are required</div>}
					<form onSubmit={this.postNews.bind(this)} method="POST">
						<div className="form-group">
							<label htmlFor="title" className="text-muted">
								Title <span className="text-danger require">*</span>
							</label>
							<input
								type="text"
								placeholder="E.g. What's Trending"
								className="form-control"
								onChange={this.onInputChange.bind(this)}
								name="title"
							/>
						</div>
						<div className="form-group">
							<label className="text-muted" htmlFor="content">
								Content
							</label>
							<textarea
								rows={5}
								className="form-control"
								name="content"
								defaultValue={''}
								onChange={this.onInputChange.bind(this)}
							/>
						</div>
						<div className="form-group">
							<div className="col-sm-6">
								<label className="text-muted">Select Category</label>
								<select
									defaultValue={country_genre}
									onChange={this.onInputChange.bind(this)}
									name="country_genre"
									className="form-control"
								>
									{countryGenreList &&
										countryGenreList.map((country_genre, i) => (
											<option key={i} value={country_genre.value}>
												{country_genre.name}
											</option>
										))}
								</select>
							</div>
							<div className="col-sm-6">
								<label className="text-muted">Select Country Genre</label>
								<select
									value={category}
									onChange={this.onInputChange.bind(this)}
									name="category"
									className="form-control"
								>
									{categoryLists &&
										categoryLists.map((category, i) => (
											<option key={i} value={category.value}>
												{category.name}
											</option>
										))}
								</select>
							</div>
						</div>
						<div className="form-group">
							<label className="text-muted">Movie Thumbnail</label>
							<div className="input-group">
								<span className="input-group-btn">
									<span onClick={this.uploadWidget.bind(this)} className="btn btn-primary btn-fill">
										Browse…
									</span>
								</span>
								<input value={this.state.filename} type="text" className="form-control" readOnly />
							</div>
						</div>
						<div className="form-group">
							<button disabled={!isValid} type="submit" className="btn btn-fill btn-success pull-right">
								Submit
							</button>
							<button className="btn btn-danger pull-left">Cancel</button>
						</div>
					</form>
					<br />
				</div>
			</div>
		);
	}
}

export default CreateNews;
