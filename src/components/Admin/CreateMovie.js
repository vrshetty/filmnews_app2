import React from "react";
import Dropzone from "react-dropzone";
import { toast } from "react-toastify";
import {
  genreList,
  countryGenreList,
  celebritiesList
} from "../../utils/lists";
import { generateYears } from "../../utils/helper";

class CreateMovie extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      movie: {
        title: "",
        description: "",
        genre: [],
        category: "",
        celebrities: [],
        year: "2018",
        created_at: new Date(),
        thumbnail: "",
        trailer_url: ""
      },
      completed: false,
      filename: "No File has been selected"
    };
  }

  onInputChange(event) {
    let state = this.state.movie;
    state[event.target.name] = event.target.value;
    this.setState(state);
  }

  uploadWidget(event) {
    let self = this;
    cloudinary.openUploadWidget(
      {
        cloud_name: 'dhftkozwd',
        upload_preset: 's49bhqbg',
        tags: ["news_photos"]
      },
      function(error, result) {
        if (!error) {
          let movie = self.state.movie;
          movie.thumbnail = result[0].secure_url;
          self.setState({
            movie,
            filename: `${result[0].original_filename}.${result[0].format}`
          });
          toast.success("Image was uploaded succesfully");
        }
      }
    );
  }

  postMovie(event) {
    event.preventDefault();
    const postData = this.state.movie;
    const { valid, result } = this.isValid(postData);
    if (valid) {
      this.props
        .saveMovies(postData)
        .then(() => toast.success(`Movie was posted successfully`));
    }
  }

  updateArray(event) {
    event.preventDefault();
    const movie = this.state.movie;
    const { value, name } = event.target;
    if (!movie[name].includes(value)) {
      movie[name] = [...movie[name], value];
    }
    this.setState({
      movie
    });
  }

  removeItem(value, name) {
    const movie = this.state.movie;
    if (movie[name].includes(value)) {
      movie[name].splice(movie[name].indexOf(value), 1);
    }
    this.setState({
      movie
    });
  }

  isValid(data) {
    const result = Object.keys(data).filter(
      key => !data[key] || data[key].length === 0
    );
    return { valid: result.length === 0, result };
  }

  render() {
    const { movie } = this.state;
    const { valid, result } = this.isValid(movie);

    return (
      <div className="row">
        <div className="col-md-12">
          <h4 className="latest-text w3_latest_text">Post a Movie</h4>
          {!valid &&
            <div className="alert alert-danger">All fields are required</div>}
          <form onSubmit={this.postMovie.bind(this)} method="POST">
            <div className="form-group">
              <label htmlFor="title" className="text-muted">
                Title <span className="text-danger require">*</span>
              </label>
              <input
                type="text"
                placeholder="E.g. What's Trending"
                className="form-control"
                onChange={this.onInputChange.bind(this)}
                name="title"
              />
            </div>
            <div className="form-group">
              <label className="text-muted" htmlFor="content">
                Content
              </label>
              <textarea
                rows={5}
                className="form-control"
                name="description"
                defaultValue={""}
                onChange={this.onInputChange.bind(this)}
              />
            </div>
            <div className="form-group">
              <div className="col-sm-6">
                <label className="text-muted">Movie Year</label>
                <select
                  onChange={this.onInputChange.bind(this)}
                  name="year"
                  className="form-control"
                  value={movie.year}
                >
                  <option> Selcct Year</option>
                  {generateYears() &&
                    generateYears().map((year, i) =>
                      <option key={i} value={year}>
                        {year}
                      </option>
                    )}
                </select>
              </div>
              <div className="col-sm-6">
                <label className="text-muted">Movie Trailer Url</label>
                <input
                  type="text"
                  placeholder="Trailer Url"
                  className="form-control"
                  onChange={this.onInputChange.bind(this)}
                  name="trailer_url"
                />
              </div>
            </div>
            <div className="form-group">
              <div className="col-sm-6">
                <label className="text-muted">Select Category</label>
                <select
                  onChange={this.onInputChange.bind(this)}
                  name="category"
                  className="form-control"
                  value={movie.country_genre}
                >
                  <option> Select Category</option>
                  {countryGenreList &&
                    countryGenreList.map((country_genre, i) =>
                      <option key={i} value={country_genre.value}>
                        {country_genre.name}
                      </option>
                    )}
                </select>
              </div>
              <div className="col-sm-6">
                <label className="text-muted">Select Genre</label>
                <select
                  value={movie.category}
                  onChange={this.updateArray.bind(this)}
                  name="genre"
                  className="form-control"
                >
                  <option> Selcct Genre</option>
                  {genreList &&
                    genreList.map((genre, i) =>
                      <option key={i} value={genre.value}>
                        {genre.name}
                      </option>
                    )}
                </select>
                <p>
                  {movie.genre &&
                    movie.genre.map((genre, i) =>
                      <span key={i} className="chip">
                        {genre}{" "}
                        <a onClick={this.removeItem.bind(this, genre, "genre")}>
                          <span>×</span>
                        </a>
                      </span>
                    )}
                </p>
              </div>
            </div>
            <div className="form-group">
              <div className="col-sm-6">
                <label className="text-muted">Add Celebrities</label>
                <select
                  onChange={this.updateArray.bind(this)}
                  name="celebrities"
                  className="form-control"
                  value={movie.celebrities}
                >
                  <option> Selcct Celebrities</option>
                  {celebritiesList &&
                    celebritiesList.map((celebrity, i) =>
                      <option key={i} value={celebrity}>
                        {celebrity}
                      </option>
                    )}
                </select>
                <p>
                  {movie.celebrities &&
                    movie.celebrities.map((celebrities, i) =>
                      <span key={i} className="chip">
                        {celebrities}{" "}
                        <a
                          onClick={this.removeItem.bind(
                            this,
                            celebrities,
                            "celebrities"
                          )}
                        >
                          <span>×</span>
                        </a>
                      </span>
                    )}
                </p>
              </div>
              <div className="col-sm-6">
                <label className="text-muted">Movie Thumbnail</label>
                <div className="input-group">
                  <span className="input-group-btn">
                    <span
                      onClick={this.uploadWidget.bind(this)}
                      className="btn btn-primary btn-fill"
                    >
                      Browse…
                    </span>
                  </span>
                  <input
                    value={this.state.filename}
                    type="text"
                    className="form-control"
                    readOnly
                  />
                </div>
              </div>
            </div>
            <div className="form-group">
              <button
                disabled={!valid}
                type="submit"
                className="btn btn-fill btn-success pull-right"
              >
                Submit
              </button>
              <button className="btn btn-danger pull-left">Cancel</button>
            </div>
          </form>
          <br />
        </div>
      </div>
    );
  }
}

export default CreateMovie;
