import React from 'react';
import moment from 'moment';
import StarRating from '../Rating/';
const Comments = ({ onChange, onSubmit, success, comment, commentsData, isAuthenticated }) => {
	return isAuthenticated ? (
		<div id="comments" className="card all-comments">
			<div className="all-comments-info">
				{success && <div className="alert alert-success">Comment was posted successfully</div>}
				<a href="#">Comments</a>
				<form onSubmit={onSubmit}>
					<div className="row">
						<div className="col-md-12">
							<div className="form-group">
								<textarea
									rows={5}
									placeholder="Write a Comment"
									id="formControlsTextarea"
									className="form-control"
									onChange={onChange}
									value={comment}
								/>
							</div>
						</div>
					</div>
					<button type="submit" className="btn-fill pull-right btn btn-danger">
						Submit
					</button>
				</form>
			</div>
			{commentsData &&
				commentsData.map((comment, i) => (
					<div className="media-grids" key={i}>
						<div className="media">
							<div className="media-left">
								<a href="#">
									<img src="/images/user.jpg" title="One movies" alt=" " />
								</a>
							</div>
							<div className="media-body">
								<h6>
									{comment.username}
									<span className="pull-right">
										{moment(comment.created_date).format('DD.MMMM.YYYY')}
									</span>
								</h6>
								<p>{comment.comment}</p>
							</div>
						</div>
					</div>
				))}
			{commentsData &&
				commentsData.length === 0 && <h2 className="no_content">No Comment yet! Be the first to post</h2>}
		</div>
	) : (
		<h2 className="no_content">SignIn to post a Comment</h2>
	);
};

export default Comments;
