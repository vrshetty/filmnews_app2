import React from "react";
// import ProgressiveImage from "react-progressive-bg-image";
import { Link } from "react-router";
import styles from "./styles.css";

const Card = ({ id, key, width, title, image, subTitle, text }) =>
  <Link
    to={`/news/${id}`}
    key={key}
    className="card"
    style={{ width: width, minHeight: 400, backgroundColor: "#453290" }}
  >
    <img className={styles.media} src={image} />
    <div className={styles.subtitle}>
      <span>
        {subTitle}
      </span>
    </div>
    <div className={styles.text}>
      <p>
        <i className="fa fa-clock-o" aria-hidden="true" /> {text}
      </p>
    </div>
  </Link>;

export default Card;
