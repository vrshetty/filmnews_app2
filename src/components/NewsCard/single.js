import React from "react";
import { Link } from "react-router";

const NewsPost = ({ news }) => {
  return (
    <div className="wthree-news-left">
      <div className="wthree-news-left-img">
        <img src="/images/7.jpg" alt />
        <h4 id="title">
          {news.title}
        </h4>
        <div className="s-author">
          <p>
            <i className="fa fa-calendar" aria-hidden="true" />{" "}
            {news.created_date}
            &nbsp;&nbsp;
            <Link to="#comments">
              <i className="fa fa-comments" aria-hidden="true" /> Comments (10)
            </Link>
          </p>
        </div>
        <div className="w3-agile-news-text">
          <p id="content">
            {news.content}
          </p>
        </div>
      </div>
    </div>
  );
};

export default NewsPost;
