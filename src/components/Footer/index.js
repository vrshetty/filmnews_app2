import React from 'react';
import { Link } from 'react-router';
import styles from './styles.css';

const Footer = () => (
	<div className="footer">
		<div id="container" className="container">
			<div className="filmifeeds_footer_grid">
				<div className="col-md-5 col-md-offset-5">
					<a href="index.html">
						<img width="80px" height="80px" src="/images/logo.jpeg" />
					</a>
				</div>
				<div className="clearfix"> </div>
			</div>
			<div className="col-md-5 filmifeeds_footer_grid1_left">
				<p>© 2018 FilmiFeed. All rights reserved</p>
			</div>
			<div className="col-md-7 filmifeeds_footer_grid1_right">
				<ul>
					<li>
						<a href="genres.html">Movies</a>
					</li>
					<li>
						<a href="faq.html">Celebrities</a>
					</li>
					<li>
						<a href="horror.html">News</a>
					</li>
					<li>
						<a href="icons.html">Support</a>
					</li>
					<li>
						<a href="contact.html">Contact Us</a>
					</li>
				</ul>
			</div>
			<div className="clearfix"> </div>
		</div>
	</div>
);

export default Footer;
