import React from "react";

import { Link } from "react-router";

const Header = ({ auth }) =>
  <div>
    <div className="header">
      <div className="container">
        <div className="col-md-2 col-sm-3 col-xs-offset-4 col-sm-offset-0">
          <Link to="/">
            <img src="/images/logo.jpeg" width="100px" height="100px" alt />
          </Link>
        </div>
        <div className="col-md-8 col-sm-6 col-xs-6 col-xs-offset-4 col-md-offset-2 w3_search">
          <form id="search_form" action="#" method="post">
            <input
              type="text"
              name="Search"
              placeholder="Search Celebs, Films and Topics"
              required
            />
            <input type="submit" defaultValue="Go" />
          </form>
          <ul style={{ display: "inline-flex", listStyle: "none" }}>
            <li style={{ paddingRight: 5 }}>
              <a>
                <i className="fa fa-facebook-square" aria-hidden="true" />
              </a>
            </li>
            <li style={{ paddingRight: 5 }}>
              <a>
                <i className="fa fa-twitter-square" aria-hidden="true" />
              </a>
            </li>
            <li style={{ paddingRight: 5 }}>
              <a>
                <i className="fa fa-youtube-square" aria-hidden="true" />
              </a>
            </li>
            {!auth.isAuthenticated &&
              <li>
                <a
                  id="login_btn"
                  href="#"
                  data-toggle="modal"
                  data-target="#myModal"
                >
                  Login
                </a>
              </li>}
            {auth.isAuthenticated &&
              <li className="dropdown">
                <a className="dropdown-toggle" data-toggle="dropdown">
                  <i className="fa fa-bars" aria-hidden="true" />
                </a>
                <ul className="dropdown-menu">
                  <li>
                    <Link to="/settings">Admin Panel</Link>
                  </li>
                  <li>
                    <Link to="/logout">Logout</Link>
                  </li>
                </ul>
              </li>}
          </ul>
        </div>
        <div className="clearfix"> </div>
      </div>
    </div>
    <div className="movies_nav">
      <div className="container">
        <nav className="navbar navbar-default">
          <div className="navbar-header navbar-left">
            <button
              type="button"
              className="navbar-toggle collapsed"
              data-toggle="collapse"
              data-target="#bs-example-navbar-collapse-1"
            >
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar" />
              <span className="icon-bar" />
              <span className="icon-bar" />
            </button>
          </div>
          {/* Collect the nav links, forms, and other content for toggling */}
          <div
            className="collapse navbar-collapse navbar-right"
            id="bs-example-navbar-collapse-1"
          >
            <nav>
              <ul className="nav navbar-nav">
                <li className="w3_home_act">
                  <Link activeClassName="active" to="/">
                    Home
                  </Link>
                </li>
                <li className="dropdown">
                  <Link
                    to="/genres"
                    activeClassName="active"
                    className="dropdown-toggle"
                    data-toggle="dropdown"
                  >
                    Genres
                  </Link>
                  {/* <ul className="dropdown-menu multi-column columns-3">
                  <li>
                    <div className="col-sm-4">
                      <ul className="multi-column-dropdown">
                        <li>
                          <A to="/" content="Action" />
                        </li>
                        <li><a href="genres.html">Biography</a></li>
                        <li><a href="genres.html">Crime</a></li>
                        <li><a href="genres.html">Family</a></li>
                        <li><a href="horror.html">Horror</a></li>
                        <li><a href="genres.html">Romance</a></li>
                        <li><a href="genres.html">Sports</a></li>
                        <li><a href="genres.html">War</a></li>
                      </ul>
                    </div>
                    <div className="col-sm-4">
                      <ul className="multi-column-dropdown">
                        <li><a href="genres.html">Adventure</a></li>
                        <li><a href="comedy.html">Comedy</a></li>
                        <li><a href="genres.html">Documentary</a></li>
                        <li><a href="genres.html">Fantasy</a></li>
                        <li><a href="genres.html">Thriller</a></li>
                      </ul>
                    </div>
                    <div className="col-sm-4">
                      <ul className="multi-column-dropdown">
                        <li><a href="genres.html">Animation</a></li>
                        <li><a href="genres.html">Costume</a></li>
                        <li><a href="genres.html">Drama</a></li>
                        <li><a href="genres.html">History</a></li>
                        <li><a href="genres.html">Musical</a></li>
                        <li><a href="genres.html">Psychological</a></li>
                      </ul>
                    </div>
                    <div className="clearfix" />
                  </li>
                </ul> */}
                </li>
                <li>
                  <Link activeClassName="active" to="/movies">
                    Movies
                  </Link>
                </li>
                <li>
                  <Link activeClassName="active" to="/news">
                    news
                  </Link>
                </li>
                <li className="dropdown">
                  <Link
                    to="#"
                    activeClassName="active"
                    className="dropdown-toggle"
                    data-toggle="dropdown"
                  >
                    Country
                  </Link>
                </li>
                <li>
                  <Link to="/celebrities">Celebrities</Link>
                </li>
              </ul>
            </nav>
          </div>
        </nav>
      </div>
    </div>
  </div>;

export default Header;
