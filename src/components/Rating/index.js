import React from "react";

const StarRating = () => {
  return (
    <div className="block-stars">
      <ul className="filmifeed-ratings">
        <li>
          <a href="#">
            <i className="fa fa-star" aria-hidden="true" />
          </a>
        </li>
        <li>
          <a href="#">
            <i className="fa fa-star" aria-hidden="true" />
          </a>
        </li>
        <li>
          <a href="#">
            <i className="fa fa-star" aria-hidden="true" />
          </a>
        </li>
        <li>
          <a href="#">
            <i className="fa fa-star" aria-hidden="true" />
          </a>
        </li>
        <li>
          <a href="#">
            <i className="fa fa-star-half-o" aria-hidden="true" />
          </a>
        </li>
      </ul>
    </div>
  );
};

export default StarRating;
