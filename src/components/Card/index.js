import React from "react";
import styles from "./styles.css";

const Card = ({ width, title, image, subTitle, text }) =>
  <div className="card" style={{ width: width }}>
    <img className={styles.media} src={image} />
    <div className={styles.subtitle}>
      <span>
        {subTitle}
      </span>
    </div>
    <div className={styles.text}>
      <p>
        <i className="fa fa-clock-o" aria-hidden="true" /> {text}
      </p>
    </div>
  </div>;

export default Card;
