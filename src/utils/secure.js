import axios from 'axios';
import jwt from 'jsonwebtoken';
import { toast } from 'react-toastify';
import { setNext, setCurrentUser } from '../actions/appActions';

export function requireAuth(store, path) {
	return function(nextState, replace) {
		const token = localStorage.getItem('token');
		if (token) {
			jwt.verify(token, process.env.jwtSecret, (error, decoded) => {
				if (!error) {
					store.dispatch(setCurrentUser(decoded));
				}
			});
		} else {
			setCurrentUser({});
			if (path) {
				store.dispatch(setNext(nextState.location.pathname));
				replace({
					pathname: path,
				});
				toast.error('You must login to access page');
			}
		}
	};
}

/**
 * setAuthorizationToken - set token to request headers
 * @param  {string} token Authorization token
 * @return {void} no return or void
 */
export function setAuthorizationToken(token) {
	if (token) {
		axios.defaults.headers.common.Authorization = `Bearer ${token}`;
	} else {
		delete axios.defaults.headers.common.Authorization;
	}
}
