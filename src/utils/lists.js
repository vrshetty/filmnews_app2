export const categoryLists = [
  {
    name: "Film News",
    value: "film_news"
  },
  {
    name: "Celebrity News",
    value: "celebrity_news"
  }
];
export const countryGenreList = [
  {
    name: "Hollywood",
    value: "hollywood"
  },
  {
    name: "Bollywood",
    value: "bollywood"
  },
  {
    name: "Kollywood",
    value: "kollywood"
  }
];
export const genreList = [
  {
    name: "Action",
    value: "action"
  },
  {
    name: "Drama",
    value: "drama"
  },
  {
    name: "Romance",
    value: "romance"
  }
];
export const celebritiesList = ["James Bond", "Jacki Chan", "Jet Li"];
