import app from './app';
import logger from './utils/logger';
import server from '../config/server.config';
import project from '../config/project.config';
import errors from './utils/errors';

import DevServer from './middleware/dev';

DevServer(app);
errors(app);

app.listen(project.port, err => {
	if (err) {
		return logger.error(err.message);
	}
	logger.started(project.port, project.ip);
});
