import express from 'express';
import Movie from '../models/Movie';
const router = express.Router();

router.get('/', function(req, res) {
	Movie.find()
		.sort({ year: 1 })
		.exec(function(err, movies) {
			if (err) {
				return res.send(err);
			}
			res.json(movies);
		});
});

router.get('/:id', function(req, res) {
	Movie.findOne(
		{
			_id: req.params.id,
		},
		function(err, movies) {
			movies.comments.reverse();
			if (err) {
				return res.send(err);
			}
			res.json(movies);
		},
	);
});

router.post('/', function(req, res) {
	const newData = {
		title: req.body.title,
		description: req.body.description,
		genre: req.body.genre,
		celebrities: req.body.celebrities,
		thumbnail: req.body.thumbnail,
		trailer_url: req.body.trailer_url,
		year: req.body.year,
		category: req.body.category,
		created_at: req.body.created_at,
	};
	const newMovie = new Movie(newData);
	newMovie.save(function(err, newMovie) {
		if (err) {
			return res
				.status(500)
				.json({
					status: 500,
					error: err,
				})
				.end();
		}
		res
			.json({
				status: 200,
				movie: newMovie,
			})
			.end();
	});
});

router.put('/:id', function(req, res) {
	var updateData = {
		title: req.body.title,
		year: req.body.year,
		director: req.body.director,
		watched: req.body.watched,
	};
	Movie.update({ _id: req.body._id }, updateData, function(err, movie) {
		if (err) {
			return res
				.status(500)
				.json({
					status: 500,
					error: err,
				})
				.end();
		}
		res
			.json({
				status: 200,
				movie: movie,
			})
			.end();
	});
});

router.patch('/:id/comment', function(request, response) {
	const comment = {
		rating: request.body.rating,
		comment: request.body.comment,
		user_name: request.body.userName,
	};
	Movie.update(
		{ _id: request.params.id },
		{
			$push: {
				comments: request.body,
			},
		},
		(error, movie) => {
			if (!error) {
				return response
					.status(200)
					.json({
						movie,
					})
					.end();
			}
			return response
				.status(500)
				.json({
					error,
				})
				.end();
		},
	);
});

router.delete('/:id', function(req, res) {
	Movie.remove({ _id: req.params.id }, function(err) {
		if (err) {
			return res
				.status(500)
				.json({
					status: 500,
					error: err,
				})
				.end();
		}
		res
			.json({
				status: 200,
				message: 'Movie successfully deleted',
			})
			.end();
	});
});

export default router;
