import express from "express";
import NewsController from "../controller/news";

const router = express.Router();
router.route("/").get(NewsController.getAll).post(NewsController.createNews);

router
  .route("/:id")
  .get(NewsController.getById)
  .put(NewsController.editNews)
  .delete(NewsController.deleteNews);

router.put("/:id/comment", NewsController.addComment);

export default router;
