import express from 'express';
import userRoutes from './user.routes';
import moviesRoutes from './movies.routes';
import newsRoutes from './news.routes';

const router = express.Router();

const RouteMiddleware = () => {
	router.get('/test', (req, res) => {
		res.send('OK yes');
	});

	router.use('/user', userRoutes);
	router.use('/movies', moviesRoutes);
	router.use('/news', newsRoutes);
	return router;
};

export default RouteMiddleware;
