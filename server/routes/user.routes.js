import { isLoggedIn, isLoggedOut } from "../middleware/login";
import express from "express";
import jwt from "jsonwebtoken";

import User from "../models/User";
import server from "../../config/server.config";

const router = express.Router();

router.post("/register", isLoggedOut, (req, res, next) => {
  const user = req.body;

  if (!user.username || !user.email || !user.password) {
    let error = new Error("Username, Email, and Password are required");
    error.status = 400;
    return next(error);
  }
  if (user.password !== user.confirm) {
    let error = new Error("Passwords must match");
    error.status = 400;
    return next(error);
  }

  User.validateUserInfo(user.username, user.email, (error, user) => {
    if (user || error) {
      return next(error);
    }
  });

  let newUser = {
    email: user.email,
    username: user.username,
    password: user.password
  };
  let data = {
    email: user.username,
    username: user.username,
    fullname: user.fullname
  };

  User.create(newUser, (error, user) => {
    if (error) {
      return next(error);
    } else {
      jwt.sign(
        { ...data, id: user._id, level: user.level },
        server.jwtSecret,
        { algorithm: "HS256", expiresIn: "1d" },
        (error, token) => {
          if (error) {
            return next(error);
          }
          res.status(200).json({
            token: token,
            data,
            message: "Success, account registered successfully"
          });
        }
      );
    }
  });
});

router.post("/login", isLoggedOut, (req, res, next) => {
  const { username, password } = req.body;
  User.authenticate(username, password, (error, user) => {
    if (error || !user) {
      let error = new Error("Invalid username or password");
      error.status = 401;
      return next(error);
    } else {
      jwt.sign(
        { username: user.username, id: user._id },
        server.jwtSecret,
        { algorithm: "HS256", expiresIn: "1d" },
        (error, token) => {
          if (error) {
            return next(error);
          }
          res
            .status(200)
            .json({
              token: token,
              message: `Success, you're logged in ${user.username}`
            });
        }
      );
    }
  });
});

router.get("/logout", isLoggedIn, (req, res, next) => {
  if (req.decoded) {
    req.decoded = null;
  }
  res.status(200).json({ message: "You've logged out" });
});

module.exports = router;
