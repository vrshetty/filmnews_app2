import mongoose from 'mongoose';
import bluebird from 'bluebird';
const { Schema } = mongoose;

mongoose.Promise = bluebird;

const commentSchema = new Schema({
	user_name: String,
	comment: String,
	rating: {
		type: Number,
		min: 0,
		max: 5,
		default: 0,
	},
});

const movieSchema = mongoose.model(
	'Movie',
	{
		title: {
			type: String,
			unique: true,
			required: true,
			dropDups: true,
		},
		year: {
			type: Number,
			default: 0,
			required: true,
		},
		category: {
			type: String,
			default: '',
		},
		description: {
			type: String,
			default: '',
			required: true,
		},
		genre: {
			type: Array,
			default: [],
			required: true,
		},
		rating: {
			type: Number,
			default: 0,
		},
		trailer_url: {
			type: String,
			default: '',
		},
		thumbnail: {
			type: String,
			default: '',
			required: true,
		},
		created_at: {
			type: String,
			default: '',
			required: true,
		},
		celebrities: {
			type: Array,
			default: [],
		},
		comments: [commentSchema],
	},
	'movies',
	{
		usePushEach: true,
	},
);

export default movieSchema;
