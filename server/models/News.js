import mongoose from "mongoose";
import bluebird from "bluebird";
mongoose.Promise = bluebird;
const newsSchema = mongoose.model(
  "News",
  {
    title: {
      type: String,
      required: true,
      dropDups: true
    },
    author: {
      type: String,
      required: true,
      default: ""
    },
    content: {
      type: String,
      required: true,
      default: ""
    },
    views: {
      type: Number,
      default: 0
    },
    featured_image: {
      type: String,
      required: true,
      default: ""
    },
    category: {
      type: String,
      required: true,
      default: ""
    },
    country_genre: {
      type: String,
      required: true,
      default: ""
    },
    comments: {
      type: Array,
      default: []
    }
  },
  "news"
);

export default newsSchema;
