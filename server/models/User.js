import bcrypt from "bcrypt";
import mongoose from "mongoose";

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
    trim: true
  },
  username: {
    type: String,
    unique: true,
    trim: true
  },
  fullname: {
    type: String,
    trim: true
  },
  password: {
    type: String
  },
  level: {
    type: Number,
    default: 2
  }
});

UserSchema.statics.authenticate = function (username, password, callback) {

  User.findOne({ username: username })
    .exec((error, user) => {
      if (error) {
        return callback(error);
      }
      if (!user) {
        let error = new Error("User not found");
        error.status = 401;
        return callback(error);
      }

      bcrypt.compare(password, user.password, function (error, match) {
        if (match) {
          return callback(null, user);
        } else if (error) {
          return callback(error);
        } else {
          let error = new Error("Credentials don't match");
          error.status = 401;
          return callback(error);
        }
      });
    });
};

UserSchema.statics.validateUserInfo = (username, email, next) => {
  User.findOne({ username })
  .exec((error, user) => {
    if (user) {
      let error = new Error("Username already exists");
      error.status = 409;
      return next(error, user);
    }
  });
  User.findOne({ email })
  .exec((error, user) => {
    if (user) {
      let error = new Error("Email Address already exists");
      error.status = 409;
      return next(error, user);
    }
  });
}

UserSchema.pre("save", function (next) {
  const user = this;
  if (!user.isModified("password")) {
    return next();
  }
  bcrypt.genSalt(10, function (error, salt) {
    bcrypt.hash(user.password, salt, function (error, hash) {
      if (error) {
        return next(error);
      }
      user.password = hash;
      next();
    });
  });
});

const User = mongoose.model("User", UserSchema);

module.exports = User;
