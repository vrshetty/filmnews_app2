import News from "../models/News";

export default {
  getAll(request, response) {
    News.find().sort({ year: 1 }).exec(function(err, news) {
      if (err) {
        return response.send(err);
      }
      response.json(news);
    });
  },

  getById(request, response) {
    News.findOne(
      {
        _id: request.params.id
      },
      null,
      { sort: { "comments.created_date": "desc" } },
      function(error, news) {
        if (!error) {
          return response.status(200).json(news).end();
        }
        return response.status(500).json(error).end();
      }
    );
  },

  createNews(request, response) {
    const {
      title,
      author,
      content,
      featured_image,
      category,
      country_genre,
      created_date
    } = request;
    const newData = {
      title,
      author,
      content,
      featured_image,
      category,
      country_genre,
      created_date
    };

    News.save((error, news) => {
      if (!error) {
        return response
          .status(200)
          .json({
            news,
            message: "News was saved successfully"
          })
          .end();
      }
      return response
        .status(500)
        .json({
          error
        })
        .end();
    });
  },

  editNews(request, response) {
    const data = {
      title: request.body.title,
      content: request.body.content,
      views: request.body.views,
      featured_image: request.body.featured_image
    };

    News.update(
      {
        _id: request.body._id
      },
      data,
      (error, news) => {
        if (!error) {
          return response
            .status(200)
            .json({
              news
            })
            .end();
        }

        return response
          .status(500)
          .json({
            error
          })
          .end();
      }
    );
  },

  addComment(request, response) {
    News.update(
      { _id: request.params.id },
      {
        $push: {
          comments: request.body
        }
      },
      (error, news) => {
        if (!error) {
          return response
            .status(200)
            .json({
              news: news
            })
            .end();
        }
        return response
          .status(500)
          .json({
            error
          })
          .end();
      }
    );
  },

  deleteNews(request, response) {
    News.remove({ _id: req.params.id }, function(error) {
      if (error) {
        return response
          .status(500)
          .json({
            error
          })
          .end();
      }
      response
        .status(200)
        .json({
          message: "News successfully deleted"
        })
        .end();
    });
  }
};
