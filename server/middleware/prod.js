/* eslint-disable */
import express from 'express';
import path from 'path';
import compression from 'compression';

const port = parseInt(process.env.PORT, 10) || 4000;

const ProdServer = app => {
	app.use(compression());
	app.use(express.static('dist'));
	app.get('*', (req, res) => {
		res.sendFile(path.join(__dirname, '../../dist/index.html'));
	});
};

export default ProdServer;
