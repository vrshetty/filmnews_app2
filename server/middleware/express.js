import compression from "compression";
import bodyParser from "body-parser";
import helmet from "helmet";
import express from "express";
import path from "path";

const AppMiddleware = (app, passport) => {
  app.use(compression());
  app.use(helmet());

  app.use(
    bodyParser.urlencoded({
      extended: false,
      limit: "20mb"
    })
  );
  app.use(bodyParser.json({ limit: "20mb" }));

  app.use(passport.initialize());
  app.use(passport.session());
  app.use(express.static(path.join(__dirname, "../../public/")));
};

export default AppMiddleware;
