import express from 'express';
import mongoose from 'mongoose';
import passport from 'passport';
import path from 'path';
import argv from 'minimist';
import BodyParser from 'body-parser';

import logger from './utils/logger';
import server from '../config/server.config';
import project from '../config/project.config';
import errors from './utils/errors';

import PassportMiddleware from './passport';
import AppMiddleware from './middleware/express';
import RouteMiddleware from './routes/';

function connect() {
	const options = { server: { socketOptions: { keepAlive: 1 } } };
	const db = mongoose.connect(server.mongoUri, options).connection;
	db.on('error', err => logger.error(err));
	db.on('open', () => logger.connected(server.mongoUri));
	return db;
}
connect();

const app = express();

PassportMiddleware(passport);
AppMiddleware(app, passport);
app.use('/api', RouteMiddleware());

export default app;
