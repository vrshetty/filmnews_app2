const path = require('path');
const compression = require('compression');
const express = require('express');
const logger = require('../build/utils/logger');
const server = require('../config/server.config');
const project = require('../config/project.config');
// const app = express();

const app = require('../build/app').default;

/* eslint-disable no-console */

app.use(compression());
app.use(express.static('dist'));

app.get('*', (req, res) => {
	res.sendFile(path.join(__dirname, '../dist/index.html'));
});

app.listen(process.env.PORT || 5000, err => {
	if (err) {
		return logger.error(err.message);
	}
	logger.started(project.port, project.ip);
});
