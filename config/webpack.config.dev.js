const argv = require('minimist');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const combineLoaders = require('webpack-combine-loaders');
const project = require('./project.config');
const server = require('./server.config');
const packageJson = require('../package.json');

module.exports = {
	name: 'client',
	target: 'web',
	entry: {
		app: ['react-hot-loader/patch', 'webpack-hot-middleware/client', project.paths.client('index.js')],
	},
	output: {
		filename: '[name].js',
		chunkFileName: '[name].chunk.js',
		path: project.paths.build(),
		publicPath: project.compiler.path,
	},
	devtool: project.compiler.devtool,
	resolve: {
		root: project.paths.client(),
		extensions: ['', '.js', '.jsx', '.json', '.css'],
	},
	module: {
		loaders: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				loader: 'babel',
				query: project.compiler.babel,
			},
			{
				test: /\.css$/,
				loader: combineLoaders([
					{
						loader: 'style-loader',
					},
					{
						loader: 'css-loader',
						query: {
							modules: true,
							localIdentName: '[name]__[local]___[hash:base64:5]',
						},
					},
				]),
			},
			{
				test: /\.svg(\?.*)?$/,
				loader: 'url-loader?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=image/svg+xml',
			},
			{ test: /\.(png|jpg|jpeg)$/, loader: 'url-loader?limit=10000' },
			{
				test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
				loader: 'file-loader',
			},
			{
				test: /\.woff2(\?\S*)?$/,
				loader: 'url-loader?limit=100000',
			},
			{
				test: /\.woff(\?\S*)?$/,
				loader: 'url-loader?limit=100000',
			},
			{
				test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
				loader: 'url-loader?limit=100000&mimetype=application/octet-stream',
			},
		],
	},
	externals: {
		'react/lib/ExecutionEnvironment': true,
		'react/lib/ReactContext': true,
		'react/addons': true,
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				apiUrl: JSON.stringify(packageJson.apiUrl),
				jwtSecret: JSON.stringify(server.jwtSecret),
			},
		}),
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoErrorsPlugin(),
		new webpack.optimize.DedupePlugin(),
		new HtmlWebpackPlugin({
			template: project.paths.client('index.html'),
			hash: false,
			favicon: project.paths.public('favicon.ico'),
			filename: 'index.html',
			inject: 'body',
		}),
	],
	node: {
		dns: 'empty',
		net: 'empty',
		fs: 'empty',
	},
};
