const argv = require('minimist');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const project = require('./project.config');
const server = require('./server.config');
const packageJson = require('../package.json');

module.exports = {
	name: 'client',
	target: 'web',
	entry: {
		app: project.paths.client('index.js'),
		vendor: project.compiler.vendors,
	},
	output: {
		filename: '[name].js',
		chunkFileName: '[name].chunk.js',
		path: project.paths.build(),
		publicPath: project.compiler.path,
	},
	devtool: project.compiler.devtool,
	resolve: {
		root: project.paths.client(),
		extensions: ['', '.js', '.jsx', '.json', '.css'],
	},
	module: {
		loaders: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				loader: 'babel',
				query: project.compiler.babel,
			},
			{
				test: /\.css$/,
				exclude: null,
				loader: ExtractTextPlugin.extract(
					'style-loader',
					'css?modules=true&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]',
				),
			},
			{
				test: /\.svg(\?.*)?$/,
				loader: 'url-loader?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=image/svg+xml',
			},
			{ test: /\.(png|jpg|jpeg)$/, loader: 'url-loader?limit=10000' },
			{
				test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
				loader: 'file-loader',
			},
			{
				test: /\.woff2(\?\S*)?$/,
				loader: 'url-loader?limit=100000',
			},
			{
				test: /\.woff(\?\S*)?$/,
				loader: 'url-loader?limit=100000',
			},
			{
				test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
				loader: 'url-loader?limit=100000&mimetype=application/octet-stream',
			},
		],
	},
	externals: {
		'react/lib/ExecutionEnvironment': true,
		'react/lib/ReactContext': true,
		'react/addons': true,
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify('production'),
				apiUrl: JSON.stringify(packageJson.appUrl),
				jwtSecret: JSON.stringify(server.jwtSecret),
			},
		}),
		new webpack.optimize.CommonsChunkPlugin({
			name: 'vendor',
			children: true,
			minChunks: 2,
			async: true,
		}),
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.optimize.UglifyJsPlugin(),
		new HtmlWebpackPlugin({
			template: project.paths.client('index.html'),
			hash: false,
			favicon: project.paths.public('favicon.ico'),
			filename: 'index.html',
			inject: 'body',
			minify: {
				removeComments: true,
				collapseWhitespace: true,
				removeRedundantAttributes: true,
				useShortDoctype: true,
				removeEmptyAttributes: true,
				removeStyleLinkTypeAttributes: true,
				keepClosingSlash: true,
				minifyJS: true,
				minifyCSS: true,
				minifyURLs: true,
			},
		}),
		new ExtractTextPlugin('styles.css'),
	],
	node: {
		dns: 'empty',
		net: 'empty',
		fs: 'empty',
	},
};
